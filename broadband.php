<?php
session_start();

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>thesmartnet.net</title>
<meta name="description" content="Place your description here">
<meta name="keywords" content="put, your, keyword, here">
<meta name="author" content="Templates.com - website templates provider">
<meta charset="utf-8">
<link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<script type="text/javascript" src="js/maxheight.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/cufon-replace.js"></script>
<script type="text/javascript" src="js/Myriad_Pro_300.font.js"></script>
<script type="text/javascript" src="js/Myriad_Pro_400.font.js"></script>
<script type="text/javascript" src="js/jquery.faded.js"></script>
<script type="text/javascript" src="js/jquery.jqtransform.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript">
	$(function(){
		$("#faded").faded({
			speed: 500,
			crossfade: true,
			autoplay: 10000,
			autopagination:false
		});
		
		$('#domain-form').jqTransform({imgPath:'jqtransformplugin/img/'});
	});
</script>
<!--[if lt IE 7]>
<script type="text/javascript" src="http://info.template-help.com/files/ie6_warning/ie6_script_other.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<![endif]-->
</head>
<body id="page1" onLoad="new ElementMaxHeight();">
<div class="tail-top">
<!-- header -->
	<header>
		<div class="container">
			<div class="header-box">
				<div class="left">
					<div class="right">
						<nav>
							<ul>
								<li><a href="index.php">Home</a></li>
								<li><a href="hosting.php">Hosting</a></li>
								<li><a href="solutions.php">Vps</a></li>
								<li class="current"><a href="support.php">Support</a></li>
								<li><a href="contacts.php">Contacts</a></li>
								<li><a href="about.php">About</a></li>
							</ul>
						</nav>
						<h1 style="padding-top: 15px"><a href="index.php">
							<img src="./images/smartnet.png" width="160"></a></h1>
					</div>
				</div>
			</div>
			<span class="top-info">24/7 Sales &amp; Support	</span>
			
				<?php
			if(!isset($_SESSION['user_id']))
			{
			?>

				<form action="login.php" id="login-form" method="post">
				<fieldset>
					<input type="hidden" value="post"  name="login" required>
					<span class="text">
						<input type="text" value=""  name="username" required>
					</span>
					<span class="text">
						<input type="password" value="" id="password" required minlength="6" name="password">
					</span>

					<a  class="login" id="btn-login"><button style="background: #428301;color: #FFF; cursor: pointer; border-radius: 5px !important"> <span><span>Login</span></span></button></a>
					<span class="links"><a href="#">Forgot Password?</a><br/><a href="register.html">Register</a></span>
				</fieldset>
			</form>

		<?php  
			}else{
		?>
				<form  id="login-form">
				<fieldset style="padding-right: 0px !important">
					<a  href="admin/index.php" class="login" id="btn-login"><button style="background: #428301;color: #FFF; cursor: pointer; border-radius: 5px !important" type="button"> <span><span>Go to Dashboard</span></span></button></a>
					
				</fieldset>
			</form>

		<?php
			}

		?>
			
		</div>
	</header>
<!-- content -->
	<section id="content"><div class="ic"></div>
		<div class="container">
			<div id="slogan">

					<div class="inside">
						<h2><span><cufon class="cufon cufon-canvas" alt=" " style="width: 5px; height: 26px;"><canvas width="25" height="30" style="width: 25px; height: 30px; top: -3px; left: -1px;"></canvas><cufontext> </cufontext></cufon><cufon class="cufon cufon-canvas" alt="BROADBAND " style="width: 162px; height: 26px;"><canvas width="181" height="30" style="width: 181px; height: 30px; top: -3px; left: -1px;"></canvas><cufontext>BROADBAND </cufontext></cufon><cufon class="cufon cufon-canvas" alt="INTERNET" style="width: 84px; height: 26px;"><canvas width="97" height="30" style="width: 97px; height: 30px; top: -3px; left: -1px;"></canvas><cufontext>INTERNET</cufontext></cufon></span><cufon class="cufon cufon-canvas" alt=" " style="width: 5px; height: 26px;"><canvas width="25" height="30" style="width: 25px; height: 30px; top: -3px; left: -1px;"></canvas><cufontext> </cufontext></cufon><cufon class="cufon cufon-canvas" alt="PLANS" style="width: 97px; height: 26px;"><canvas width="105" height="30" style="width: 105px; height: 30px; top: -3px; left: -1px;"></canvas><cufontext>PLANS</cufontext></cufon></h2>
					
						<p>Experience the internet at the fastest speeds with our fiber optic broadband. Enjoy a seamless connection, perfect for connecting multiple devices over WiFi and LAN. With our fiber optic technology, you will get superfast connectivity and the best broadband internet speeds at all times.</p>
					</div>
				</div>
				<br>
			<div class="inside">
				<div class="wrapper row-1">
					<div class="box col-1 maxheight">
						<div class="border-right maxheight">
							<div class="border-bot maxheight">
								<div class="border-left maxheight">
									<div class="left-top-corner maxheight">
										<div class="right-top-corner maxheight">
											<div class="right-bot-corner maxheight">
												<div class="left-bot-corner maxheight">
													<div class="inner">
														<h3>Basic Plan</h3>
														<ul class="info-list">
															<li><span>Speed</span>25 Mbs</li>
															<li><span>Data Limit</span>50 Gb</li>
															<li><span>Calls Limit</span>5 Gb</li>
															
														</ul>
														<span class="price">$ 9.95 p/m</span>
														<div class="aligncenter"><a href="<?=(!$_SESSION['user_id']?'stripe' : 'register.html' )?>" class="link1"><span><span>Buy Now</span></span></a></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box col-2 maxheight">
						<div class="border-right maxheight">
							<div class="border-bot maxheight">
								<div class="border-left maxheight">
									<div class="left-top-corner maxheight">
										<div class="right-top-corner maxheight">
											<div class="right-bot-corner maxheight">
												<div class="left-bot-corner maxheight">
													<div class="inner">
														<h3>Economy Plan</h3>
														<ul class="info-list">
															<li><span>Speed</span>50 Gb</li>
															<li><span>Data Limit</span>100 Gb</li>
															<li><span>Calls Limit</span>10 Gb</li>
														</ul>
														<span class="price">$ 14.95 p/m</span>
														<div class="aligncenter"><a href="<?=(!$_SESSION['user_id']?'stripe' : 'register.html' )?>" class="link1"><span><span>Buy Now</span></span></a></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box col-3 maxheight">
						<div class="border-right maxheight">
							<div class="border-bot maxheight">
								<div class="border-left maxheight">
									<div class="left-top-corner maxheight">
										<div class="right-top-corner maxheight">
											<div class="right-bot-corner maxheight">
												<div class="left-bot-corner maxheight">
													<div class="inner">
														<h3>Deluxe Plan</h3>
														<ul class="info-list">
															<li><span>Speed</span>150 Mbs</li>
															<li><span>Data Limit</span>150 Gb</li>
															<li><span>Calls Limit</span>20 Gb</li>
														</ul>
														<span class="price">$ 25.95 p/m</span>
														<div class="aligncenter"><a href="<?=(!$_SESSION['user_id']?'stripe' : 'register.html' )?>" class="link1"><span><span>Buy Now</span></span></a></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box col-4 maxheight">
						<div class="border-right maxheight">
							<div class="border-bot maxheight">
								<div class="border-left maxheight">
									<div class="left-top-corner maxheight">
										<div class="right-top-corner maxheight">
											<div class="right-bot-corner maxheight">
												<div class="left-bot-corner maxheight">
													<div class="inner">
														<h3>Unlimited Plan</h3>
														<ul class="info-list">
															<li><span>Speed</span>200 Mbs</li>
															<li><span>Data Limit</span>200 Gb</li>
															<li><span>Calls Limit</span>30 Gb</li>
														</ul>
														<span class="price">$ 30.95 p/m</span>
														<div class="aligncenter"><a href="<?=(!$_SESSION['user_id']?'stripe' : 'register.html' )?>" class="link1"><span><span>Buy Now</span></span></a></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="inside1">
					<div class="wrap row-2">
						<article class="col-1">
							<h2>Solutions</h2>
							<ul class="solutions">
								<li><img src="images/icon1.gif"><p>Quickly and easily create a Web Page</p></li>
								<li><img src="images/icon2.gif"><p>Share documents any time, any where</p></li>
								<li><img src="images/icon3.gif"><p>24/7 Real Person Customer Support</p></li>
								<li><img src="images/icon4.gif"><p>Online Account Management Tools</p></li>
							</ul>
						</article>
						<article class="col-2">
							<h3>Simple plans, no surprises</h3>
							<p style="text-align: justify;">
								We're all about keeping it simple: no hidden charges or surprises on your bill. What you see is what you get.</p>

					
						</article>
						<article class="col-2">
							<h3>Simple plans, no surprises</h3>

								<p style="text-align: justify;">
								Enjoy the flexibility of our month by month option or save on a great value contract. It’s your choice to decide what’s right for you.</p>
						</article>
						<article class="col-2">
							<h3>No peak or off peak limits</h3>
								<p style="text-align: justify;">
								We don't believe in complicated peak and off peak periods. So you're free to use your data your way, whenever you want.</p>
						</article>

						<article class="col-2">
							<h3>Everything you need online</h3>
								<p style="text-align: justify;">
								Set up, connect and manage your service with ease, all online. We're all about making it easy for you to find what you need and help yourself in your own time..</p>
						</article>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<!-- aside -->
<aside>
	<div class="container">
		<div class="inside">
			<div class="line-ver1">
				<div class="line-ver2">
					<div class="line-ver3">
						<div class="wrapper line-ver4">
							<ul class="list col-1" >
								<li>Account Manager</li>
								<li><a href="#">Dashboard</a></li>
								<li><a href="#">Account Settings </a></li>
								<li><a href="#">Billing</a></li>
								
							</ul>
							<ul class="list col-2">
								<li>Shopping</li>
								<li><a href="hosting.php">Hosting</a></li>
								<li><a href="solutions.php">VPS</a></li>
								<li><a href="broadband.php">Broadband</a></li>
								<li><a href="email.php">Email Hosting</a></li>
							</ul>
						
							<ul class="list col-4">
								<li>Help and Support</li>
								<li><a href="contacts.php">Support &amp; Sales</a></li>
								<li><a href="support_billing.php">Billing Support</a></li>
								<li><a href="support_email.php">Email Our Support Team</a></li>
							</ul>
							<ul class="list col-5">
								<li>About</li>
								<li><a href="about.php">About TheSmartNet</a></li>
								<li><a href="terms.html" target="blank">Terms and Conditions</a></li>
								<li><a href="privacy_policy.html" target="blank">Privacy Policy</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</aside>
<!-- footer -->
<footer>
	<div class="container">
		
	</div>
</footer>
<style>
	input:invalid,
	textarea:invalid {
	    background: red;
	}

	#login-form .btn-login  {
	    padding: 5px 22px 6px 22px;
	    background: url(../images/button-right1.gif) no-repeat right top;
	}

	#login-form .login{
	    display: block;
	    background: url(../images/button-left1.gif) no-repeat left top;
	}

	aside .inside {
	    padding: 35px 100px 35px 125px;
	}
</style>

<script type="text/javascript"> 

	Cufon.now(); 

	function link_hosting(){
		location.href = 'hosting.html'
	}

	function link_dedicate(){
		location.href = 'solutions.html'
	}
</script>

</body>
</html>