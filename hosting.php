<?php
session_start();


?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>thesmartnet.net</title>
  <meta name="description" content="Place your description here">
  <meta name="keywords" content="put, your, keyword, here">
  <meta name="author" content="Templates.com - website templates provider">
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
  <link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
  <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
  <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
  <script type="text/javascript" src="js/cufon-yui.js"></script>
  <script type="text/javascript" src="js/cufon-replace.js"></script>
  <script type="text/javascript" src="js/Myriad_Pro_300.font.js"></script>
  <script type="text/javascript" src="js/Myriad_Pro_400.font.js"></script>
  <script type="text/javascript" src="js/Myriad_Pro_600.font.js"></script>
  <script type="text/javascript" src="js/script.js"></script>
  <!--[if lt IE 7]>
<script type="text/javascript" src="http://info.template-help.com/files/ie6_warning/ie6_script_other.js"></script>
 <![endif]-->
  <!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<![endif]-->
</head>

<body id="page3">
  <div class="tail-top2">
    <!-- header -->
    <header>
      <div class="container">
       <div class="header-box">
        <div class="left">
          <div class="right">
            <nav>
              <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="hosting.php">Hosting</a></li>
                <li><a href="solutions.php">Vps</a></li>
                <li class="current"><a href="support.php">Support</a></li>
                <li><a href="contacts.php">Contacts</a></li>
                <li><a href="about.php">About</a></li>
              </ul>
            </nav>
          <h1 style="padding-top: 15px"><a href="index.php">
              <img src="./images/smartnet.png" width="160"></a></h1>
          </div>
        </div>
      </div>
      <span class="top-info">24/7 Sales &amp; Support </span>
     
  <?php
      if(!isset($_SESSION['user_id']))
      {
      ?>

        <form action="login.php" id="login-form" method="post">
        <fieldset>
          <input type="hidden" value="post"  name="login" required>
          <span class="text">
            <input type="text" value=""  name="username" required>
          </span>
          <span class="text">
            <input type="password" value="" id="password" required minlength="6" name="password">
          </span>

          <a  class="login" id="btn-login"><button style="background: #428301;color: #FFF; cursor: pointer; border-radius: 5px !important"> <span><span>Login</span></span></button></a>
          <span class="links"><a href="#">Forgot Password?</a><br/><a href="register.html">Register</a></span>
        </fieldset>
      </form>

    <?php 
      }else{
    ?>
        <form  id="login-form">
        <fieldset style="padding-right: 0px !important">
          <a  href="admin/index.php" class="login" id="btn-login"><button style="background: #428301;color: #FFF; cursor: pointer; border-radius: 5px !important" type="button"> <span><span>Go to Dashboard</span></span></button></a>
          
        </fieldset>
      </form>

    <?php
      }

    ?>
      </div>
    </header>
    <!-- content -->
    <section id="content">
      <div class="ic"></div>
      <div class="container">
        <div class="inside bot-indent">
          <div id="slogan">
            <div class="inside">
              <h2><span>Your Domain Name</span> Helps the World  to Find You</h2>
              <p>t is a service that provides greater flexibility, availability and scalability. Flexibility because at any time you can increase or decrease resources in real time (CPU, RAM, SSD disk space, transfer). Availability because all elements are redundant and in case of any problem, a new server is created (replica of the old one). This replica goes live in less than a minute.</p>
            </div>
          </div>
          <h2 class="extra">Hosting Plans</h2>
          <div class="box extra">
            <div class="border-right">
              <div class="border-bot">
                <div class="border-left">
                  <div class="left-top-corner1">
                    <div class="right-top-corner1">
                      <div class="right-bot-corner">
                        <div class="left-bot-corner">
                          <div class="inner">
                            <div class="left-indent line-ver1">
                              <div class="line-ver2">
                               
                                  <div class="wrap line-ver4">
                                    <article class="col-1 indent">
                                      <h4>Hosting Plans</h4>
                                      <ul class="info-list1">
                                        <li>Disk Space</li>
                                        <li>Bandwidth</li>
                                        <li>Monthly Price</li>
                                        <li>99.9% uptime</li>
                                        <li>24/7 support</li>
                                        <li>Daily backups</li>
                                        <li>No Contract</li>
                                        <li>30 day money back guarantee</li>
                                        <li>Free Setup</li>
                                        <li>Order</li>
                                      </ul>
                                    </article>
                                    <article class="col-2 indent">
                                      <h4 class="aligncenter">Personal</h4>
                                      <ul class="info-list1 alt">
                                        <li>2,000 MB</li>
                                        <li>50,000 mb</li>
                                        <li>$35.00</li>
                                        <li>yes</li>
                                        <li>yes</li>
                                        <li>yes</li>
                                        <li>yes</li>
                                        <li>yes</li>
                                        <li>yes</li>
                                      </ul>
                                      <div class="aligncenter"><a href="<?=(!$_SESSION['user_id']?'stripe' : 'register.html' )?>" class="link3"><span><span>Buy Now!</span></span></a></div>
                                    </article>
                                    <article class="col-1 indent">
                                      <h4 class="aligncenter">Standard</h4>
                                      <ul class="info-list1 alt">
                                        <li>3,000mb</li>
                                        <li>65,000mb</li>
                                        <li>$45.00</li>
                                        <li>yes</li>
                                        <li>yes</li>
                                        <li>yes</li>
                                        <li>yes</li>
                                        <li>yes</li>
                                        <li>yes</li>
                                      </ul>
                                      <div class="aligncenter"><a href="<?=(!$_SESSION['user_id']?'stripe' : 'register.html' )?>" class="link3"><span><span>Buy Now!</span></span></a></div>
                                    </article>
                                    <article class="col-2 indent">
                                      <h4 class="aligncenter">Advanced</h4>
                                      <ul class="info-list1 alt">
                                        <li>4,000mb</li>
                                        <li>100,00mb</li>
                                        <li>$65.00</li>
                                        <li>yes</li>
                                        <li>yes</li>
                                        <li>yes</li>
                                        <li>yes</li>
                                        <li>yes</li>
                                        <li>yes</li>
                                      </ul>
                                      <div class="aligncenter"><a href="<?=(!$_SESSION['user_id']?'stripe' : 'register.html' )?>" class="link3"><span><span>Buy Now!</span></span></a></div>
                                    </article>

                                    <div class="clear"></div>
                                  </div>
                             
                              </div>
                            </div>
                            <div class="border-top">
                              <div class="inner1">
                                <h4 class="extra aligncenter">You can create...</h4>
                                <div class="">
                                  <div class="line-ver1 left-indent">
                                    <div class="line-ver2">
                                      <div class="line-ver3">
                                        <div class="wrap line-ver4">
                                          <article class="col-1 indent1">
                                            <ul class="info-list1">
                                              <li>Domains</li>
                                              <li>Subdomains</li>
                                              <li>FTP accounts</li>
                                            </ul>
                                          </article>
                                          <article class="col-2 indent1">
                                            <ul class="info-list1 alt">
                                              <li>unlimited</li>
                                              <li>unlimited</li>
                                              <li>unlimited</li>
                                            </ul>
                                          </article>
                                          <article class="col-3 indent1">
                                            <ul class="info-list1 alt">
                                              <li>unlimited</li>
                                              <li>unlimited</li>
                                              <li>unlimited</li>
                                            </ul>
                                          </article>
                                          <article class="col-4 indent1">
                                            <ul class="info-list1 alt">
                                              <li>unlimited</li>
                                              <li>unlimited</li>
                                              <li>unlimited</li>
                                            </ul>
                                          </article>
                                          <article class="col-5 indent1">
                                            <ul class="info-list1 alt">
                                              <li>unlimited</li>
                                              <li>unlimited</li>
                                              <li>unlimited</li>
                                            </ul>
                                          </article>
                                          <div class="clear"></div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="border-top">
                              <div class="inner1">
                                <h4 class="extra aligncenter">All accounts will have...</h4>
                                <div class="">
                                  <div class="line-ver1 left-indent">
                                    <div class="line-ver2">
                                      <div class="line-ver3">
                                        <div class="wrap line-ver4">
                                          <article class="col-1 indent1">
                                            <ul class="info-list1">
                                              <li>MySQL Databases</li>
                                              <li>CGI</li>
                                              <li>PERL</li>
                                              <li>Cron</li>
                                              <li>SSI</li>
                                              <li>Frontpage</li>
                                              <li>Curl</li>
                                              <li>Image Magick</li>
                                              <li>Streaming video and music</li>
                                              <li>GD</li>
                                              <li>Python</li>
                                              <li>PHP</li>
                                            </ul>
                                          </article>
                                          <article class="col-2 indent1">
                                            <ul class="info-list1 alt">
                                              <li>unlimited</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                            </ul>
                                          </article>
                                          <article class="col-3 indent1">
                                            <ul class="info-list1 alt">
                                              <li>unlimited</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                            </ul>
                                          </article>
                                          <article class="col-4 indent1">
                                            <ul class="info-list1 alt">
                                              <li>unlimited</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                            </ul>
                                          </article>
                                          <article class="col-5 indent1">
                                            <ul class="info-list1 alt">
                                              <li>unlimited</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                              <li>yes</li>
                                            </ul>
                                          </article>
                                          <div class="clear"></div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- aside -->
<aside>
  <div class="container">
    <div class="inside">
      <div class="line-ver1">
        <div class="line-ver2">
          <div class="line-ver3">
            <div class="wrapper line-ver4">
              <ul class="list col-1" >
                <li>Account Manager</li>
                <li><a href="#">Dashboard</a></li>
                <li><a href="#">Account Settings </a></li>
                <li><a href="#">Billing</a></li>
                
              </ul>
              <ul class="list col-2">
                <li>Shopping</li>
                <li><a href="hosting.php">Hosting</a></li>
                <li><a href="solutions.php">VPS</a></li>
                <li><a href="broadband.php">Broadband</a></li>
                <li><a href="email.php">Email Hosting</a></li>
              </ul>
            
              <ul class="list col-4">
                <li>Help and Support</li>
                <li><a href="contacts.php">Support &amp; Sales</a></li>
                <li><a href="support_billing.php">Billing Support</a></li>
                <li><a href="support_email.php">Email Our Support Team</a></li>
              </ul>
              <ul class="list col-5">
                <li>About</li>
                <li><a href="about.php">About TheSmartNet</a></li>
                <li><a href="terms.html" target="blank">Terms and Conditions</a></li>
                <li><a href="privacy_policy.html" target="blank">Privacy Policy</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</aside>
  <!-- footer -->
  <footer>
    <div class="container">
     
    </div>
  </footer>
  <script type="text/javascript">
    Cufon.now();
  </script>

  <style>
    aside .inside {
      padding: 35px 100px 35px 125px;
    }
  </style>
</body>

</html>