<?php
session_start();


?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>thesmartnet.net</title>
<meta name="description" content="Place your description here">
<meta name="keywords" content="put, your, keyword, here">
<meta name="author" content="Templates.com - website templates provider">
<meta charset="utf-8">
<link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/cufon-replace.js"></script>
<script type="text/javascript" src="js/Myriad_Pro_300.font.js"></script>
<script type="text/javascript" src="js/Myriad_Pro_400.font.js"></script>
<script type="text/javascript" src="js/Myriad_Pro_600.font.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<!--[if lt IE 7]>
<script type="text/javascript" src="http://info.template-help.com/files/ie6_warning/ie6_script_other.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<![endif]-->
</head>
<body id="page5">
<div class="tail-top1">
<!-- header -->
	<header>
		<div class="container">
			<div class="header-box">
				<div class="left">
					<div class="right">
						<nav>
							<ul>
								<li><a href="index.php">Home</a></li>
								<li><a href="hosting.php">Hosting</a></li>
								<li><a href="solutions.php">Vps</a></li>
								<li class="current"><a href="support.php">Support</a></li>
								<li><a href="contacts.php">Contacts</a></li>
								<li><a href="about.php">About</a></li>
							</ul>
						</nav>
					<h1 style="padding-top: 15px"><a href="index.php">
							<img src="./images/smartnet.png" width="160"></a></h1>
					</div>
				</div>
			</div>
			<span class="top-info">24/7 Sales &amp; Support	</span>
			
				<?php
			if(!isset($_SESSION['user_id']))
			{
			?>

				<form action="login.php" id="login-form" method="post">
				<fieldset>
					<input type="hidden" value="post"  name="login" required>
					<span class="text">
						<input type="text" value=""  name="username" required>
					</span>
					<span class="text">
						<input type="password" value="" id="password" required minlength="6" name="password">
					</span>

					<a  class="login" id="btn-login"><button style="background: #428301;color: #FFF; cursor: pointer; border-radius: 5px !important"> <span><span>Login</span></span></button></a>
					<span class="links"><a href="#">Forgot Password?</a><br/><a href="register.html">Register</a></span>
				</fieldset>
			</form>

		<?php
			}else{
		?>
				<form  id="login-form">
				<fieldset style="padding-right: 0px !important">
					<a  href="admin/index.php" class="login" id="btn-login"><button style="background: #428301;color: #FFF; cursor: pointer; border-radius: 5px !important" type="button"> <span><span>Go to Dashboard</span></span></button></a>
					
				</fieldset>
			</form>

		<?php
			}

		?>
			
		</div>
	</header>
	<!-- content -->
	<section id="content"><div class="ic"></div>
		<div class="container">
			<div class="inside">
				<div id="slogan">
					<div class="inside">
						<h2><span>
				MAINTENANCE SERVICE</span>  AND SUPPORT OF HOSTING</h2>
						<p>Always keep your virtual private server in the best conditions with the maintenance and support plan of Actio Procesos & Tecnología. Make sure you have a fast and efficient support for your VPS capable of answering questions quickly and keeping the system always ready to go..</p>
					</div>
				</div>
				<ul class="banners wrapper">
					<li><a href="#">Basic  &nbsp; <b>$19.95</b></a></li>
					<li><a href="#">Economy  &nbsp; <b>$24.95</b></a></li>
					<li><a href="#">Deluxe  &nbsp; <b>$40.95</b></a></li>
					<li><a href="#">Unlimited  &nbsp; <b>$80.95</b></a></li>
				</ul>
				<div class="inside1">
					<div class="">
						
						<article class="col-4">
							<h2><cufon class="cufon cufon-canvas" alt="Special " style="width: 85px; height: 30px;"><canvas width="107" height="34" style="width: 107px; height: 34px; top: -3px; left: -1px;"></canvas><cufontext>Special </cufontext></cufon><cufon class="cufon cufon-canvas" alt="Support " style="width: 98px; height: 30px;"><canvas width="120" height="34" style="width: 120px; height: 34px; top: -3px; left: -1px;"></canvas><cufontext>Support </cufontext></cufon><cufon class="cufon cufon-canvas" alt="Programs" style="width: 107px; height: 30px;"><canvas width="124" height="34" style="width: 124px; height: 34px; top: -3px; left: -1px;"></canvas><cufontext>Programs</cufontext></cufon></h2>
							<div class="img-box"><img src="images/5page-img.jpg"><p style="text-align: justify;">Ideally suited for web hosting companies or data centers having a large number of domains and servers to be catered to. A dedicated support team is assigned to your company and they work only for you with our outsourced IT support services. All our staff members are well trained Microsoft Certified Engineers, Red Hat Certified Engineers, Cisco Certified Engineers with a minimum of 3 to 4 years of experience and they are dedicated to work only for your company.

Under Dedicated Technical Team plan, a Dedicated Support Team of certified engineers is assigned to work on your servers, tickets and live chats. This team is constantly monitored by an IT Operations Manager to ensure the quality levels, monitoring reports analysis, patch management, upgrades, etc.</p></div>
								
							<p class="p0">.</p>
						</article>
						<article class="col-2">
							<h2><cufon class="cufon cufon-canvas" alt="Features" style="width: 107px; height: 30px;"><canvas width="124" height="34" style="width: 124px; height: 34px; top: -3px; left: -1px;"></canvas><cufontext>Features</cufontext></cufon></h2>
							<div class="img-box"><p style="text-align: justify;">
								<ol type="circle">

									<li>1. Each shared engineer works 8 hours a day and 5 days a week</li>
									<li>2. Well defined internal escalation policy</li>
									<li>3. Full server monitoring and server administration included</li>
									<li>4. Full Support for pre-sales questions / tickets</li>
									<li>5. Unlimited Helpdesk or email support</li>
									</ol></p></div>
								
							<p class="p0">.</p>
						</article>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<!-- aside -->
<aside>
	<div class="container">
		<div class="inside">
			<div class="line-ver1">
				<div class="line-ver2">
					<div class="line-ver3">
						<div class="wrapper line-ver4">
							<ul class="list col-1" >
								<li>Account Manager</li>
								<li><a href="#">Dashboard</a></li>
								<li><a href="#">Account Settings </a></li>
								<li><a href="#">Billing</a></li>
								
							</ul>
							<ul class="list col-2">
								<li>Shopping</li>
								<li><a href="hosting.php">Hosting</a></li>
								<li><a href="solutions.php">VPS</a></li>
								<li><a href="broadband.php">Broadband</a></li>
								<li><a href="email.php">Email Hosting</a></li>
							</ul>
						
							<ul class="list col-4">
								<li>Help and Support</li>
								<li><a href="contacts.php">Support &amp; Sales</a></li>
								<li><a href="support_billing.php">Billing Support</a></li>
								<li><a href="support_email.php">Email Our Support Team</a></li>
							</ul>
							<ul class="list col-5">
								<li>About</li>
								<li><a href="about.php">About TheSmartNet</a></li>
								<li><a href="terms.html" target="blank">Terms and Conditions</a></li>
								<li><a href="privacy_policy.html" target="blank">Privacy Policy</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</aside>
<!-- footer -->
<footer>
	<div class="container">
		
	</div>
</footer>
<script type="text/javascript"> Cufon.now(); </script>
<style>
	aside .inside {
	    padding: 35px 100px 35px 125px;
	}
</style>
</body>
</html>