<?php
session_start();


?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>thesmartnet.net</title>
<meta name="description" content="Place your description here">
<meta name="keywords" content="put, your, keyword, here">
<meta name="author" content="Templates.com - website templates provider">
<meta charset="utf-8">
<link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<script type="text/javascript" src="js/maxheight.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/cufon-replace.js"></script>
<script type="text/javascript" src="js/Myriad_Pro_300.font.js"></script>
<script type="text/javascript" src="js/Myriad_Pro_400.font.js"></script>
<script type="text/javascript" src="js/jquery.faded.js"></script>
<script type="text/javascript" src="js/jquery.jqtransform.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript">
	$(function(){
		$("#faded").faded({
			speed: 500,
			crossfade: true,
			autoplay: 10000,
			autopagination:false
		});
		
		$('#domain-form').jqTransform({imgPath:'jqtransformplugin/img/'});
	});
</script>
<!--[if lt IE 7]>
<script type="text/javascript" src="http://info.template-help.com/files/ie6_warning/ie6_script_other.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<![endif]-->
</head>
<body id="page1" onLoad="new ElementMaxHeight();">
<div class="tail-top">
<!-- header -->
	<header>
		<div class="container">
			<div class="header-box">
				<div class="left">
					<div class="right">
						<nav>
							<ul>
								<li class="current"><a href="index.php">Home</a></li>
								<li><a href="hosting.php">Hosting</a></li>
								<li><a href="solutions.php">Vps</a></li>
								<li><a href="support.php">Support</a></li>
								<li><a href="contacts.php">Contacts</a></li>
								<li><a href="about.php">About</a></li>
							</ul>
						</nav>

						<h1 style="padding-top: 15px"><a href="index.php">
							<img src="./images/smartnet.png" width="160"></a></h1>
					</div>
				</div>
			</div>
			<span class="top-info">24/7 Sales &amp; Support	</span>
				<?php
			if(!isset($_SESSION['user_id']))
			{
			?>

				<form action="login.php" id="login-form" method="post">
				<fieldset>
					<input type="hidden" value="post"  name="login" required>
					<span class="text">
						<input type="text" value=""  name="username" required>
					</span>
					<span class="text">
						<input type="password" value="" id="password" required minlength="6" name="password">
					</span>

					<a  class="login" id="btn-login"><button style="background: #428301;color: #FFF; cursor: pointer; border-radius: 5px !important"> <span><span>Login</span></span></button></a>
					<span class="links"><a href="#">Forgot Password?</a><br/><a href="register.html">Register</a></span>
				</fieldset>
			</form>

		<?php
			}else{
		?>
				<form  id="login-form">
				<fieldset style="padding-right: 0px !important">
					<a  href="admin/index.php" class="login" id="btn-login"><button style="background: #428301;color: #FFF; cursor: pointer; border-radius: 5px !important" type="button"> <span><span>Go to Dashboard</span></span></button></a>
					
				</fieldset>
			</form>

		<?php
			}

		?>
		</div>
	</header>
<!-- content -->
	<section id="content"><div class="ic"></div>
		<div class="container">
			<div id="slogan">
					<div class="inside">
						<h2><span><cufon class="cufon cufon-canvas" alt=" " style="width: 5px; height: 26px;"><canvas width="25" height="30" style="width: 25px; height: 30px; top: -3px; left: -1px;"></canvas><cufontext> </cufontext></cufon><cufon class="cufon cufon-canvas" alt="VPS " style="width: 162px; height: 26px;"><canvas width="181" height="30" style="width: 181px; height: 30px; top: -3px; left: -1px;"></canvas><cufontext>VPS </cufontext></cufon><cufon class="cufon cufon-canvas" alt="DEDICATED" style="width: 84px; height: 26px;"><canvas width="97" height="30" style="width: 97px; height: 30px; top: -3px; left: -1px;"></canvas><cufontext>DEDICATED</cufontext></cufon></span><cufon class="cufon cufon-canvas" alt=" " style="width: 5px; height: 26px;"><canvas width="25" height="30" style="width: 25px; height: 30px; top: -3px; left: -1px;"></canvas><cufontext> </cufontext></cufon><cufon class="cufon cufon-canvas" alt="HOSTING" style="width: 97px; height: 26px;"><canvas width="105" height="30" style="width: 105px; height: 30px; top: -3px; left: -1px;"></canvas><cufontext>HOSTING</cufontext></cufon></h2>
					
						<p>If you dedicate yourself to the development, design or administration of systems, you have experience in server management and you are looking for total control of your hosting environment, our self-managed VPS Hosting will be a great acquisition. You will have full access to the root with SSH keys to keep control of users, as well as command lines for greater flexibility and power. In addition, we offer you a wide range of server configurations and scalable performance options.</p>
					</div>
				</div>
				<br>
			<div class="inside">
				<div class="wrapper row-1">
					<div class="box col-1 maxheight">
						<div class="border-right maxheight">
							<div class="border-bot maxheight">
								<div class="border-left maxheight">
									<div class="left-top-corner maxheight">
										<div class="right-top-corner maxheight">
											<div class="right-bot-corner maxheight">
												<div class="left-bot-corner maxheight">
													<div class="inner">
														<h3>Basic Plan</h3>
														<ul class="info-list">
															<li><span>Ram</span>16 Gb</li>
															<li><span>Disk space</span>1 Tb</li>
															<li><span>FTP accounts</span>25</li>
															<li><span>Email boxes</span>1000</li>
															<li><span>Proxmox</span></li>
														</ul>
														<span class="price">$ 19.95 p/m</span>
														<div class="aligncenter"><a href="<?=(!$_SESSION['user_id']?'stripe' : 'register.html' )?>" class="link1"><span><span>Buy Now</span></span></a></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box col-2 maxheight">
						<div class="border-right maxheight">
							<div class="border-bot maxheight">
								<div class="border-left maxheight">
									<div class="left-top-corner maxheight">
										<div class="right-top-corner maxheight">
											<div class="right-bot-corner maxheight">
												<div class="left-bot-corner maxheight">
													<div class="inner">
														<h3>Economy Plan</h3>
														<ul class="info-list">
															<li><span>Ram</span>16 Gb</li>
															<li><span>Disk space</span>1 Tb</li>
															<li>
															<li><span>FTP accounts</span>50</li>
															<li><span>Email boxes</span>2500</li>
															<li><span>Proxmox</span></li>
														</ul>
														<span class="price">$ 24.95 p/m</span>
														<div class="aligncenter"><a href="<?=(!$_SESSION['user_id']?'stripe' : 'register.html' )?>" class="link1"><span><span>Buy Now</span></span></a></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box col-3 maxheight">
						<div class="border-right maxheight">
							<div class="border-bot maxheight">
								<div class="border-left maxheight">
									<div class="left-top-corner maxheight">
										<div class="right-top-corner maxheight">
											<div class="right-bot-corner maxheight">
												<div class="left-bot-corner maxheight">
													<div class="inner">
														<h3>Deluxe Plan</h3>
														<ul class="info-list">
															<li><span>Ram</span>32 Gb</li>
															<li><span>Disk space</span>2 Tb</li>
															<li><span>FTP accounts</span>70</li>
															<li><span>Email boxes</span>3000</li>
															<li><span>Proxmox</span></li>
														</ul>
														<span class="price">$ 45.95 p/m</span>
														<div class="aligncenter"><a href="<?=(!$_SESSION['user_id']?'stripe' : 'register.html' )?>" class="link1"><span><span>Buy Now</span></span></a></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box col-4 maxheight">
						<div class="border-right maxheight">
							<div class="border-bot maxheight">
								<div class="border-left maxheight">
									<div class="left-top-corner maxheight">
										<div class="right-top-corner maxheight">
											<div class="right-bot-corner maxheight">
												<div class="left-bot-corner maxheight">
													<div class="inner">
														<h3>Unlimited Plan</h3>
														<ul class="info-list">
															<li><span>Ram</span>64 Gb</li>
															<li><span>Disk space</span>2 Tb</li>
															<li><span>FTP accounts</span>Unlimited</li>
															<li><span>Email boxes</span>Unlimited</li>
															<li><span>Proxmox</span></li>
														</ul>
														<span class="price">$ 80.95 p/m</span>
														<div class="aligncenter"><a href="<?=(!$_SESSION['user_id']?'stripe' : 'register.html' )?>" class="link1"><span><span>Buy Now</span></span></a></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="inside1">
					<div class="wrap row-2">
						<article class="col-1">
							<h2>Solutions</h2>
							<ul class="solutions">
								<li><img src="images/icon1.gif"><p>Quickly and easily create a Web Page</p></li>
								<li><img src="images/icon2.gif"><p>Share documents any time, any where</p></li>
								<li><img src="images/icon3.gif"><p>24/7 Real Person Customer Support</p></li>
								<li><img src="images/icon4.gif"><p>Online Account Management Tools</p></li>
							</ul>
						</article>
						<article class="col-2">
							<h2>Self-managed VPS hosting</h2>
							<p style="text-align: justify;">

Yes, VPS Hosting is the best option for web developers and system administrators who have enough technical knowledge to manage a server. A virtual server offers more power and flexibility than standard shared hosting, and therefore the complexity of its management is greater. But if what you are looking for is the power of VPS with the simplicity of standard cPanel to manage your sites, then we recommend our plans.</p>

							<p style="text-align: justify;">
								<ol type="circle">

									<li>1. Each shared engineer works 8 hours a day and 5 days a week</li>
									<li>2. Well defined internal escalation policy</li>
									<li>3. Full server monitoring and server administration included</li>
									<li>4. Full Support for pre-sales questions / tickets</li>
									<li>5. Unlimited Helpdesk or email support</li>
									</ol></p>
						</article>

						<article class="col-2">
							<h2>Advantages of using vps</h2>
							<p style="text-align: justify;">
								<ol type="circle">

									<li style="text-align: justify;">1. <b>Total autonomy</b>
In addition to allowing you to configure the server according to your particular needs, you will also have full control over the administration panel, and you will be able to install or run the applications you want at any time</li>
									<li style="text-align: justify;">2. 
<b>Load balancing</b>
A good VPS will allow you to optimize the availability of the server, regardless of its location, and thereby avoid data loss.</li>
									<li style="text-align: justify;">3. <b>IP adicionales</b> a
One help that a quality VPS can give you is the possibility of displaying additional geolocated IP addresses. So your website and apps will always be available where your customers and users are, increasing your international SEO strategy.</li>
									</ol></p>
						</article>

						<div class="clear"></div>

					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<aside>
	<div class="container">
		<div class="inside">
			<div class="line-ver1">
				<div class="line-ver2">
					<div class="line-ver3">
						<div class="wrapper line-ver4">
							<ul class="list col-1" >
								<li>Account Manager</li>
								<li><a href="#">Dashboard</a></li>
								<li><a href="#">Account Settings </a></li>
								<li><a href="#">Billing</a></li>
								
							</ul>
							<ul class="list col-2">
								<li>Shopping</li>
								<li><a href="hosting.php">Hosting</a></li>
								<li><a href="solutions.php">VPS</a></li>
								<li><a href="broadband.php">Broadband</a></li>
								<li><a href="email.php">Email Hosting</a></li>
							</ul>
						
							<ul class="list col-4">
								<li>Help and Support</li>
								<li><a href="contacts.php">Support &amp; Sales</a></li>
								<li><a href="support_billing.php">Billing Support</a></li>
								<li><a href="support_email.php">Email Our Support Team</a></li>
							</ul>
							<ul class="list col-5">
								<li>About</li>
								<li><a href="about.php">About TheSmartNet</a></li>
								<li><a href="terms.html" target="blank">Terms and Conditions</a></li>
								<li><a href="privacy_policy.html" target="blank">Privacy Policy</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</aside>
<!-- footer -->
<footer>
	<div class="container">
		
	</div>
</footer>
<style>
	input:invalid,
	textarea:invalid {
	    background: red;
	}

	#login-form .btn-login  {
	    padding: 5px 22px 6px 22px;
	    background: url(../images/button-right1.gif) no-repeat right top;
	}

	#login-form .login{
	    display: block;
	    background: url(../images/button-left1.gif) no-repeat left top;
	}
	aside .inside {
	    padding: 35px 100px 35px 125px;
	}


}
</style>

<script type="text/javascript"> 

	Cufon.now(); 

	function link_hosting(){
		location.href = 'hosting.html'
	}

	function link_dedicate(){
		location.href = 'solutions.html'
	}
</script>

</body>
</html>