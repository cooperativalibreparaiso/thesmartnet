<?php
include('config.php');

session_start();

if(isset($_SESSION['user_id']))
{
    header("Location: index.php");
    exit;
}

 
if (isset($_POST['login'])) {
 
    $username = $_POST['username'];
    $password = $_POST['password'];

    $sql = "SELECT * FROM users WHERE email='".$username."'";

 
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_array($result);


     if(mysqli_num_rows($result) <= 0){
 
        $error="Username no exists!";
        header('Location: error.php?error='.$error);
      
    } else {

        if (password_verify($password , $row['password'])) {
     
            $_SESSION['user_id'] = $row['id'];
            header('Location: index.php');
        } else {
            $error="Password incorrect!";
            header('Location: error.php?error='.$error);
        }
    }
}
 
?>