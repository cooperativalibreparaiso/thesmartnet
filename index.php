<?php
session_start();


?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>thesmartnet.net</title>
<meta name="description" content="Place your description here">
<meta name="keywords" content="put, your, keyword, here">
<meta name="author" content="Templates.com - website templates provider">
<meta charset="utf-8">
<link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<script type="text/javascript" src="js/maxheight.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/cufon-replace.js"></script>
<script type="text/javascript" src="js/Myriad_Pro_300.font.js"></script>
<script type="text/javascript" src="js/Myriad_Pro_400.font.js"></script>
<script type="text/javascript" src="js/jquery.faded.js"></script>
<script type="text/javascript" src="js/jquery.jqtransform.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript">
	$(function(){
		$("#faded").faded({
			speed: 500,
			crossfade: true,
			autoplay: 10000,
			autopagination:false
		});
		
		//$('#domain-form').jqTransform({imgPath:'jqtransformplugin/img/'});
	});
</script>
<!--[if lt IE 7]>
<script type="text/javascript" src="http://info.template-help.com/files/ie6_warning/ie6_script_other.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<![endif]-->
</head>
<body id="page1" onLoad="new ElementMaxHeight();">
<div class="tail-top">
<!-- header -->
	<header>
		<div class="container">
			<div class="header-box">
				<div class="left">
					<div class="right">
						<nav>
							<ul>
								<li><a href="index.php">Home</a></li>
								<li><a href="hosting.php">Hosting</a></li>
								<li><a href="solutions.php">Vps</a></li>
								<li class="current"><a href="support.php">Support</a></li>
								<li><a href="contacts.php">Contacts</a></li>
								<li><a href="about.php">About</a></li>
							</ul>
						</nav>
						<h1 style="padding-top: 15px"><a href="index.php">
							<img src="./images/smartnet.png" width="160"></a></h1>
					</div>
				</div>
			</div>
			<span class="top-info">24/7 Sales &amp; Support	</span>
			
			<?php
			if(!isset($_SESSION['user_id']))
			{
			?>

				<form action="login.php" id="login-form" method="post">
				<fieldset>
					<input type="hidden" value="post"  name="login" required>
					<span class="text">
						<input type="text" value=""  name="username" required>
					</span>
					<span class="text">
						<input type="password" value="" id="password" required minlength="6" name="password">
					</span>

					<a  class="login" id="btn-login"><button style="background: #428301;color: #FFF; cursor: pointer; border-radius: 5px !important"> <span><span>Login</span></span></button></a>
					<span class="links"><a href="#">Forgot Password?</a><br/><a href="register.html">Register</a></span>
				</fieldset>
			</form>

		<?php 
		  }else{
		?>
				<form  id="login-form">
				<fieldset style="padding-right: 0px !important">
					<a  href="admin/index.php" class="login" id="btn-login"><button style="background: #428301;color: #FFF; cursor: pointer; border-radius: 5px !important" type="button"> <span><span>Go to Dashboard</span></span></button></a>
					
				</fieldset>
			</form>

		<?php
			}
		?>
		</div>
	</header>
<!-- content -->
	<section id="content"><div class="ic"></div>
		<div class="container">
			<div id="faded">
				<ul class="slides">
					<li><img src="images/slide-title1.gif"><a href="hosting.php"><span><span><cufon class="cufon cufon-canvas" alt="Get " style="width: 26px; height: 15px;"><canvas width="38" height="18" style="width: 38px; height: 18px; top: -2px; left: -1px;"></canvas><cufontext>Get </cufontext></cufon><cufon class="cufon cufon-canvas" alt="More" style="width: 30px; height: 15px;"><canvas width="34" height="18" style="width: 34px; height: 18px; top: -2px; left: -1px;"></canvas><cufontext>More</cufontext></cufon></span></span></a></li>

					<li><img src="images/slide-title4.gif"><a href="broadband.php"><span><span><cufon class="cufon cufon-canvas" alt="Get " style="width: 26px; height: 15px;"><canvas width="38" height="18" style="width: 38px; height: 18px; top: -2px; left: -1px;"></canvas><cufontext>Get </cufontext></cufon><cufon class="cufon cufon-canvas" alt="More" style="width: 30px; height: 15px;"><canvas width="34" height="18" style="width: 34px; height: 18px; top: -2px; left: -1px;"></canvas><cufontext>More </cufontext></cufon></span></span></a></li>

					<li><img src="images/slide-title3.gif"><a href="email.php"><span><span><cufon class="cufon cufon-canvas" alt="Get " style="width: 26px; height: 15px;"><canvas width="38" height="18" style="width: 38px; height: 18px; top: -2px; left: -1px;"></canvas><cufontext>Get </cufontext></cufon><cufon class="cufon cufon-canvas" alt="More" style="width: 30px; height: 15px;"><canvas width="34" height="18" style="width: 34px; height: 18px; top: -2px; left: -1px;"></canvas><cufontext>More</cufontext></cufon></span></span></a></li>

				<li><img src="images/slide-title2.gif"><a href="solutions.php"><span><span><cufon class="cufon cufon-canvas" alt="Get " style="width: 26px; height: 15px;"><canvas width="38" height="18" style="width: 38px; height: 18px; top: -2px; left: -1px;"></canvas><cufontext>Get </cufontext></cufon><cufon class="cufon cufon-canvas" alt="More" style="width: 30px; height: 15px;"><canvas width="34" height="18" style="width: 34px; height: 18px; top: -2px; left: -1px;"></canvas><cufontext>More</cufontext></cufon></span></span></a></li>

				</ul>
				<ul class="pagination">
					<li><a href="#" rel="0" onclick="link_hosting()"><span>Web Hosting</span><small>Get more information</small></a></li>
					<li><a href="" rel="1" onclick="link_broadband()"><span>Broadband</span><small>Get more information</small></a></li>
					<li><a href="#" rel="2" onclick="link_email()"><span>Email Hosting</span><small>Get more information</small></a></li>
					<li><a href="#" rel="3" onclick="link_dedicate()"><span>Dedicated</span><small>Get more information</small></a></li>
				</ul>
			</div>


			<div class="inside">
      
        <div id="pricing-table" class="clear">
            <div class="plan">
                <h2>Personal</h2>
                <ul>
                    <li>Disk Space <b>2000 Mb</b> </li>
                    <li>Bandwidth <b>50000 Mb</b> </li>
                    <li>Monthly Price <b> 35 $</b> </li>
                    <li>99.9% uptime <b>Yes</b> </li>
                    <li>24/7 support <b>Yes</b> </li>
                    <li>Daily backups <b>Yes</b> </li>
                    <li>No Contract <b>Yes</b> </li>
                    <li>Free Setup <b>Yes</b> </li>   
                </ul>
                <span><h1>$35</h1></span>

					<a class="signup" href="<?=(!$_SESSION['user_id']?'stripe' : 'register.html' )?>">Buy Now</a>
            </div>
            <div class="plan">
                <h2>Standard</h2>
                <ul>
                    <li>Disk Space <b>3000 Mb</b> </li>
                    <li>Bandwidth <b>65000 Mb</b> </li>
                    <li>Monthly Price <b>45 $</b> </li>
                    <li>99.9% uptime <b>Yes</b> </li>
                    <li>24/7 support <b>Yes</b> </li>
                    <li>Daily backups <b>Yes</b> </li>
                    <li>No Contract <b>Yes</b> </li>
                    <li>Free Setup <b>Yes</b> </li>   
                </ul>
                <span><h1>$45</h1></span>
               <a class="signup" href="<?=(!$_SESSION['user_id']?'stripe' : 'register.html' )?>">Buy Now</a>
            </div>  
            <div class="plan">
                <h2>Avanced</h2>
                <ul>
                    <li>Disk Space <b>4000 Mb</b> </li>
                    <li>Bandwidth <b>100000 Mb</b> </li>
                    <li>Monthly Price <b>65 $</b> </li>
                    <li>99.9% uptime <b>Yes</b> </li>
                    <li>24/7 support <b>Yes</b> </li>
                    <li>Daily backups <b>Yes</b> </li>
                    <li>No Contract <b>Yes</b> </li>
                    <li>Free Setup <b>Yes</b> </li>   
                </ul>
                <span><h1>$65</h1></span>
               <a class="signup" href="<?=(!$_SESSION['user_id']?'stripe' : 'register.html' )?>">Buy Now</a>
            </div>  
        </div>
       
				<div class="inside1">
					<div class="wrap row-2">
						<article class="col-1">
							<h2>Solutions</h2>
							<ul class="solutions">
								<li><img src="images/icon1.gif"><p>Quickly and easily create a Web Page</p></li>
								<li><img src="images/icon2.gif"><p>Share documents any time, any where</p></li>
								<li><img src="images/icon3.gif"><p>24/7 Real Person Customer Support</p></li>
								<li><img src="images/icon4.gif"><p>Online Account Management Tools</p></li>
							</ul>
						</article>
						<article class="col-2">
							<h2>Register Domain Name</h2>
							<form action="buscador.php" id="domain-form" method="post">
								<div class="img-box"><img src="images/1page-img.jpg">
									<div class="extra-wrap">
										<fieldset>
											<span class="text">
												<input type="text" value="" id="domain" name="domain" placeholder="enter domain name" required>
											</span>
											<ul class="checkboxes wrapper">
												<li><input type="radio" name="ext" required value=".mx"><span> .mx</span></li>
												<li><input type="radio" name="ext" required value=".net"><span> .net</span></li>
												<li><input type="radio" name="ext" value=".com"><span> .com</span></li>
												<li><input type="radio" name="ext" value=".eu"><span> .eu</span></li>
												<li><input type="radio" name="ext" value=".us.com"><span> .us.com</span></li>
												<li><input type="radio" name="ext" value=".info"><span> .info</span></li>
												<li><input type="radio" name="ext" value=".mobi"><span> .mobi</span></li>
												<li><input type="radio" name="ext" value=".co.uk"><span> .co.uk</span></li>
												
											</ul>
										</fieldset>
									</div>
								</div>
								<div id="loader">

								</div>
							
								<button id="enviar" style="background: #428301;color: #FFF; cursor: pointer; border-radius: 5px !important">Check  Domain</button>
									
								
							</form>
							<h2>Your Domain Name Helps the World  to Find You</h2>
							<p style="text-align: justify;">
If you are looking for credibility and stability in your Hosting and VPS provider, you will be delighted to know that we are the right provider. This means that thousands of people trust us to host their sites and applications.</p>
							<p style="text-align: justify;">We offer you a VPS Hosting with a high-performance KVM and configurations adaptable to each budget, including automatic weekly and on-demand backups, monitoring, alert tools and expert technical support.</p>
							
						</article>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<!-- aside -->
<aside>
	<div class="container">
		<div class="inside">
			<div class="line-ver1">
				<div class="line-ver2">
					<div class="line-ver3">
						<div class="wrapper line-ver4">
							<ul class="list col-1" >
								<li>Account Manager</li>
								<li><a href="#">Dashboard</a></li>
								<li><a href="#">Account Settings </a></li>
								<li><a href="#">Billing</a></li>
								
							</ul>
							<ul class="list col-2">
								<li>Shopping</li>
								<li><a href="hosting.php">Hosting</a></li>
								<li><a href="solutions.php">VPS</a></li>
								<li><a href="broadband.php">Broadband</a></li>
								<li><a href="email.php">Email Hosting</a></li>
							</ul>
						
							<ul class="list col-4">
								<li>Help and Support</li>
								<li><a href="contacts.php">Support &amp; Sales</a></li>
								<li><a href="support_billing.php">Billing Support</a></li>
								<li><a href="support_email.php">Email Our Support Team</a></li>
							</ul>
							<ul class="list col-5">
								<li>About</li>
								<li><a href="about.php">About TheSmartNet</a></li>
								<li><a href="terms.html" target="blank">Terms and Conditions</a></li>
								<li><a href="privacy_policy.html" target="blank">Privacy Policy</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</aside>
<!-- footer -->
<footer>
	<div class="container">
		
	</div>
</footer>
<style>
	input:invalid,
	textarea:invalid {
	    background: red;
	}

	#login-form .btn-login  {
	    padding: 5px 22px 6px 22px;
	    background: url(../images/button-right1.gif) no-repeat right top;
	}

	#login-form .login{
	    display: block;
	    background: url(../images/button-left1.gif) no-repeat left top;
	}

	aside .inside {
	    padding: 35px 100px 35px 125px;
	}
body{
  background: #303030;
}

#pricing-table {
  margin: 10px auto;
  text-align: center;
  width: 1092px; /* total computed width = 222 x 3 + 226 */
}

#pricing-table .plan {
  font: 12px 'Lucida Sans', 'trebuchet MS', Arial, Helvetica;
  text-shadow: 0 1px rgba(255,255,255,.8);        
  background: #fff;      
  border: 1px solid #ddd;
  color: #333;
  padding: 20px;
  width: 284px; /* plan width = 180 + 20 + 20 + 1 + 1 = 222px */      
  float: left;
  position: relative;
}

#pricing-table #most-popular {
  z-index: 2;
  top: -13px;
  border-width: 3px;
  padding: 30px 20px;
  -moz-border-radius: 5px;
  -webkit-border-radius: 5px;
  border-radius: 5px;
  -moz-box-shadow: 20px 0 10px -10px rgba(0, 0, 0, .15), -20px 0 10px -10px rgba(0, 0, 0, .15);
  -webkit-box-shadow: 20px 0 10px -10px rgba(0, 0, 0, .15), -20px 0 10px -10px rgba(0, 0, 0, .15);
  box-shadow: 20px 0 10px -10px rgba(0, 0, 0, .15), -20px 0 10px -10px rgba(0, 0, 0, .15);    
}

#pricing-table .plan:nth-child(1) {
  -moz-border-radius: 5px 0 0 5px;
  -webkit-border-radius: 5px 0 0 5px;
  border-radius: 5px 0 0 5px;        
}

#pricing-table .plan:nth-child(4) {
  -moz-border-radius: 0 5px 5px 0;
  -webkit-border-radius: 0 5px 5px 0;
  border-radius: 0 5px 5px 0;        
}

/* --------------- */ 

#pricing-table h3 {
  font-size: 20px;
  font-weight: normal;
  padding: 20px;
  margin: -20px -20px 50px -20px;
  background-color: #eee;
  background-image: -moz-linear-gradient(#fff,#eee);
  background-image: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#eee));    
  background-image: -webkit-linear-gradient(#fff, #eee);
  background-image: -o-linear-gradient(#fff, #eee);
  background-image: -ms-linear-gradient(#fff, #eee);
  background-image: linear-gradient(#fff, #eee);
}

#pricing-table #most-popular h3 {
  background-color: #ddd;
  background-image: -moz-linear-gradient(#eee,#ddd);
  background-image: -webkit-gradient(linear, left top, left bottom, from(#eee), to(#ddd));    
  background-image: -webkit-linear-gradient(#eee, #ddd);
  background-image: -o-linear-gradient(#eee, #ddd);
  background-image: -ms-linear-gradient(#eee, #ddd);
  background-image: linear-gradient(#eee, #ddd);
  margin-top: -30px;
  padding-top: 30px;
  -moz-border-radius: 5px 5px 0 0;
  -webkit-border-radius: 5px 5px 0 0;
  border-radius: 5px 5px 0 0;     
}

#pricing-table .plan:nth-child(1) h3 {
  -moz-border-radius: 5px 0 0 0;
  -webkit-border-radius: 5px 0 0 0;
  border-radius: 5px 0 0 0;       
}

#pricing-table .plan:nth-child(4) h3 {
  -moz-border-radius: 0 5px 0 0;
  -webkit-border-radius: 0 5px 0 0;
  border-radius: 0 5px 0 0;       
} 

#pricing-table h3 span {
  display: block;
  font: bold 25px/100px Georgia, Serif;
  color: #777;
  background: #fff;
  border: 5px solid #fff;
  height: 100px;
  width: 100px;
  margin: 10px auto -65px;
  -moz-border-radius: 100px;
  -webkit-border-radius: 100px;
  border-radius: 100px;
  -moz-box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;
  -webkit-box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;
  box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;
}

/* --------------- */

#pricing-table ul {
  margin: 20px 0 0 0;
  padding: 0;
  list-style: none;
}

#pricing-table li {
  border-top: 1px solid #ddd;
  padding: 10px 0;
}

/* --------------- */
  
#pricing-table .signup {
  position: relative;
  padding: 8px 20px;
  margin: 20px 0 0 0;  
  color: #fff;
  font: bold 14px Arial, Helvetica;
  text-transform: uppercase;
  text-decoration: none;
  display: inline-block;       
  background-color: #72ce3f;
  background-image: -moz-linear-gradient(#72ce3f, #62bc30);
  background-image: -webkit-gradient(linear, left top, left bottom, from(#72ce3f), to(#62bc30));    
  background-image: -webkit-linear-gradient(#72ce3f, #62bc30);
  background-image: -o-linear-gradient(#72ce3f, #62bc30);
  background-image: -ms-linear-gradient(#72ce3f, #62bc30);
  background-image: linear-gradient(#72ce3f, #62bc30);
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
  border-radius: 3px;     
  text-shadow: 0 1px 0 rgba(0,0,0,.3);        
  -moz-box-shadow: 0 1px 0 rgba(255, 255, 255, .5), 0 2px 0 rgba(0, 0, 0, .7);
  -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, .5), 0 2px 0 rgba(0, 0, 0, .7);
  box-shadow: 0 1px 0 rgba(255, 255, 255, .5), 0 2px 0 rgba(0, 0, 0, .7);
}

#pricing-table .signup:hover {
  background-color: #62bc30;
  background-image: -moz-linear-gradient(#62bc30, #72ce3f);
  background-image: -webkit-gradient(linear, left top, left bottom, from(#62bc30), to(#72ce3f));      
  background-image: -webkit-linear-gradient(#62bc30, #72ce3f);
  background-image: -o-linear-gradient(#62bc30, #72ce3f);
  background-image: -ms-linear-gradient(#62bc30, #72ce3f);
  background-image: linear-gradient(#62bc30, #72ce3f); 
}

#pricing-table .signup:active, #pricing-table .signup:focus {
  background: #62bc30;       
  top: 2px;
  -moz-box-shadow: 0 0 3px rgba(0, 0, 0, .7) inset;
  -webkit-box-shadow: 0 0 3px rgba(0, 0, 0, .7) inset;
  box-shadow: 0 0 3px rgba(0, 0, 0, .7) inset; 
}

/* --------------- */

.clear:before, .clear:after {
  content:"";
  display:table
}

.clear:after {
  clear:both
}

.clear {
  zoom:1
}


</style>

<script type="text/javascript"> 

	Cufon.now(); 

	function link_hosting(){
		location.href = 'hosting.php'
	}

	function link_dedicate(){
		location.href = 'solutions.php'
	}

	function link_broadband(){
		location.href = 'broadband.php'
	}

	function link_email(){
		location.href = 'email.php'
	}
</script>

</body>
</html>