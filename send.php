<?php
// Impotar algunas clases de phpmailer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Cargar phpmailer via composer
require 'vendor/autoload.php';

$mail = new PHPMailer(true);

if(empty($_POST['email'])){
	$error="Email required!";
    header('Location: error.php?error='.$error);
}

if(empty($_POST['message'])){
	$error="Message required!";
    header('Location: error.php?error='.$error);
}

try {
    //Si el correo no te llega, quita el comentario
    //de la linea de abajo, para mas información
    $mail->SMTPDebug = SMTP::DEBUG_SERVER;
    $mail->isSMTP();
    $mail->Host       = 'smtp.mailtrap.io';
    $mail->SMTPAuth   = true;
    $mail->Username   = 'd3fe8026163a9d';
    $mail->Password   = '8ba3cb9cf175d5';
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
    $mail->Port       = 2525;

    //Destinatarios
    $mail->setFrom('franquiciabluemoney@gmail.com');
    $mail->addAddress($_POST['email']);

    // Contenido
    //$mail->isHTML(true);                          
    $mail->Subject = 'Contact Support';
    $mail->Body    = $_POST['message'];
    //$mail->AltBody = 'Version en texto plano del correo (No HTML, no formato)';


    if($mail->send()){
    	$msg="Email send successfull!";
    	header('Location: success.php?msg='.$msg);
	}else{
		$error="Email send fails!";
    	header('Location: error.php?error='.$error);
	}


} catch (Exception $e) {
    $error=$mail->ErrorInfo;
    header('Location: error.php?error='.$error);
}